-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

1bb62a39adf66a160b192037dcb31c34bc83ac9746ebdd34c48d2e9929f9ac76  bitcoin-cash-node-28.0.0-aarch64-linux-gnu-debug.tar.gz
226c1fa200136db34a26d17a65dda9ef9e18c306ad4fd7ae360253053eb3e334  bitcoin-cash-node-28.0.0-aarch64-linux-gnu.tar.gz
2b45ab5d1448dcb62863a33e8a3a24f2c91f4ad1797cd3305bab4649b6e81764  bitcoin-cash-node-28.0.0-x86_64-linux-gnu-debug.tar.gz
ba735cd3b70fab35ac1496e38596cec1f8d34989924376de001d4a86198f7158  bitcoin-cash-node-28.0.0-x86_64-linux-gnu.tar.gz
599f629a4d782abe7a064ed7897dfc2291461869d01243df1d49ba8cc66badbb  src/bitcoin-cash-node-28.0.0.tar.gz

1c0b804ede869b18d7265182b843ad8f2e818196f965939864214d3c47bd681d  bitcoin-cash-node-28.0.0-win-unsigned.tar.gz
3c4c307038e557f5f1e804fe85bfe54c7d38a2b6444562cf98ba337424887fe1  bitcoin-cash-node-28.0.0-win64-debug.zip
b04e0cfb9168ea1c06b5789980dddc5e653eda4f8c4af44370f1cb67356ff259  bitcoin-cash-node-28.0.0-win64-setup-unsigned.exe
565088703e7b35ae69cf9d837cb4a159222911dd1aa4d063ad5c1560d3f84a61  bitcoin-cash-node-28.0.0-win64.zip

608ea756e679ce971f6b62a15e80695c270b3f9311240e1d7f4a8f35d1627ded  bitcoin-cash-node-28.0.0-osx-unsigned.dmg
500f6da99a7353b22eae7937ad55cbaa43e197a3e207f2bbcfa0ec65aa7fbbe7  bitcoin-cash-node-28.0.0-osx-unsigned.tar.gz
a0caaeecc5d14dcf6c4cf85e6707a96fc6e8a0d950db0dac3b2144ec4844b7bf  bitcoin-cash-node-28.0.0-osx64.tar.gz
-----BEGIN PGP SIGNATURE-----

iJMEARYKADsWIQS2RbNcKx2arE0WbiUb5N/ATXLCgQUCZ0pZEB0caW1fdW5hbWVA
Yml0Y29pbmNhc2hub2RlLm9yZwAKCRAb5N/ATXLCgUfSAQCbmmdlyTqZ7T+AsXWm
qnUzy4Yl+GP64adb7nAQMBSbNgD/WuuLNRElzAM0+K+fWwmCAovVJMo7hgPxRLmt
HgxOngM=
=VRac
-----END PGP SIGNATURE-----
