BCHN Financial Report 2022-12-08
================================

The Bitcoin Cash Node (BCHN) project practices and promotes transparent
reporting.

Project funds are held by team members in a 3-of-5 multi-signature wallet.

This wallet is used to receive [donations](https://bitcoincashnode.org/#donate)
and pay for project expenses (general funding of BCH Node operations,
including personnel, equipment and contracting).

All our spending transactions are on the Bitcoin Cash (BCH) blockchain
except when we dispose of non-BCH coins, received through airdrops or
other means, which we may convert into BCH.

We use open source wallet software ([Electron Cash](https://www.electroncash.org/))
and maintain our accounts using [plain text accounting](https://plaintextaccounting.org/)
in a [ledger file](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public/-/blob/master/finance/bookkeeping/bchn.journal)
which is part of the public
[project management repository](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public)
on GitLab.

Based on this public ledger, the project can account for all its income and
expenses and produce financial reports using auditable open source software.

Please refer to the Links section at the bottom for links to previous
financial reports.


Project account details as of 2022-12-08
----------------------------------------

The statements below cover transactions up to and including on December 08, 2022
(more precisely, up to block 770,107).

3188.93124188 BCH was held by the project at the time of writing.
This represents an decrease in available funds of 1235.13339528 BCH since the
previous report in June 2022.
The project remains in a good financial state.

Since project start, a total of 2578.28881336 BCH has been spent so far,
broken down as indicated.

                          assets:crypto
          0.13141780 ABC    bchabc
       3188.93124188 BCH    bitcoincash
       2578.28881336 BCH  expenses
       2164.41198537 BCH    development
         87.97578591 BCH      backporting
         40.54174199 BCH      bounty
          0.25000000 BCH        doc
          0.50000000 BCH        feature
          6.09334702 BCH        review
         11.00000000 BCH        security
          6.75000000 BCH        test
         15.94839497 BCH        website
         62.65460295 BCH      daa
       1932.22806063 BCH      general
          5.71632624 BCH      general verification
         35.29546765 BCH      website
          6.80363425 BCH        flipstarter
         20.54222098 BCH        general
          7.94961242 BCH        i18n
        249.75377220 BCH    external:verification:scaling
          7.98748784 BCH    infrastructure
          5.10000000 BCH    marketing
                            mining fees
          0.00846524 BCH      bch
          0.00705620 ABC      bchabc
        148.77246084 BCH    pr
        146.77246084 BCH      representative
          2.00000000 BCH      video
          2.25464187 BCH    translation
                          income
      -5602.51852793 BCH    donation
         -0.16663168 ABC    donation_replay
         -5.09999336 BCH    refund:marketing
      -2063.99958445 ABC  equity:abc_fork


Income
------

Currently BCHN derives its income entirely from donations.

The donations below include those made directly to our project donation
address and those forwarded from tips on this site.

A total of 5.83834617 BCH in new donations was received during this reporting period.
A small amount of previously donated 1000 USDT was converted to BCH
during this period, resulting in another 5.51186990 BCH, for a total income of
11.35021607 BCH during the period.

A negligible amount of BCHA/ABC/XEC) was received as a result of
replayed BCH donation transactions (this can be seen in the replay journal
file).

A total of 5602.51852793 BCH had been received in donations since the start
of the project in February 2020. About 17% of that was raised via the original Flipstarter campaign which [successfully completed](https://bitcoincashnode.org/en/newsroom/bchn-flipstarted) on 26 April 2020, yielding 978 BCH in funding for BCHN's initial [project proposal](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public/-/raw/master/finance/funding/campaigns/2020-q2-flipstarter/pdf/Bitcoin_Cash_Node_Flipstarter_Funding_Proposal_v1_1.pdf).
Thereafter some big anonymous donations have been received:

- 1000 BCH in August 2020
- 1000 BCH in January 2021
- 1000 BCH in February 2021
- 1000 BCH in November 2021
-  200 BCH in May 2022

The remainder was received from several business and personal donations.
Some of our biggest known donors include ASICseer.com, Marc de Mesel,
Georg Engelmann and 'zveda'.

We thank everyone who has generously donated to us during the time since
our last report.


Expenses
--------

Below are the project's expenses since the last overview on June 7, 2022.
They amount to 1246.48344103 BCH over that period, or about 208 BCH / month
(compared to ~ 203 BCH / month over the previous report's period).

The mining fees have been excluded.

    2022-07-02 ex:development:website:general      4.35626804 BCH
    2022-07-02 ex:development:general              9.95916741 BCH
    2022-07-02 ex:development:general             14.93428912 BCH
    2022-07-02 ex:development:general            119.31987670 BCH
    2022-07-02 ex:development:general             21.03960396 BCH
    2022-07-02 ex:pr:representative               12.01923077 BCH
    2022-07-07 ex:external:verification:scaling   73.48300971 BCH
    2022-08-01 ex:development:general             18.95306859 BCH
    2022-08-01 ex:development:general             47.84775386 BCH
    2022-08-01 ex:pr:representative                9.05797101 BCH
    2022-08-03 ex:external:verification:scaling   33.13239886 BCH
    2022-09-01 ex:development:general             86.39545056 BCH
    2022-09-01 ex:pr:representative               10.86956522 BCH
    2022-09-01 ex:development:general             13.15789474 BCH
    2022-09-09 ex:external:verification:scaling   51.20294916 BCH
    2022-10-01 ex:development:general             50.57803400 BCH
    2022-10-02 ex:development:general             30.70472641 BCH
    2022-10-04 ex:pr:representative               10.50420200 BCH
    2022-11-01 ex:pr:representative               10.82063711 BCH
    2022-11-01 ex:development:general             82.25820417 BCH
    2022-11-01 ex:development:general            129.84764542 BCH
    2022-11-01 ex:development:general            102.17391304 BCH
    2022-11-01 ex:development:general             10.86956521 BCH
    2022-11-01 ex:development:general             36.02620087 BCH
    2022-11-30 ex:development:general             11.06194690 BCH
    2022-12-01 ex:development:general             88.02032266 BCH
    2022-12-01 ex:pr:representative               11.16071429 BCH
    2022-12-01 ex:development:general             17.29910714 BCH
    2022-12-01 ex:development:general             52.45535714 BCH
    2022-12-02 ex:development:general             13.63636364 BCH
    2022-12-02 ex:development:general             59.36073059 BCH
    2022-12-05 ex:development:general              3.40909091 BCH
    2022-12-05 ex:development:website:general      0.56818182 BCH

In the time since the last report, BCHN activity was directed towards:

- review and implementation of CHIPs and related application changes
  for the May 2023 Bitcoin Cash network upgrade
- bug fixing
- optimization
- improvement of tests and benchmarks
- code cleanup
- general backports
- technical investigations and reports on various node client
  software architectures
- user support

No bounties were awarded in this period, but a bounty process is
in development.

64.4323204 BCH were spent on user relations, covering six monthly
payments to our representative. This amount is relatively higher (in BCH)
due to the worse conversion rate of USD/BCH during this period.

1019.30831304 BCH were spent on client software development and maintenance.

4.92444986 BCH were spent on website maintenance and content.

157.81835773 BCH were spent on an external contract with Software Verde
to conduct scaling tests with various software infrastructure components.
This is a concluded contract which ran from 1 April 2022 to 31 August,
with final payment being made on 9 September 2022.
It is categorised as 'external' in the above expenses.
Software Verde reported their contractual progress on this work on the
Bitcoin Cash Research site.


Basic statistics about BCHN project ledger
------------------------------------------

These are derived from hledger tool's statistic report.

    Main file                : bchn.journal
    Included files           : prnc2exht3zxlrqqcat690tc85cvfuypngh7szx6mk.journal
                               pp03e95qz7yene7v4vdx2vfsqx4qsq4wfvw8z3pkjf.journal
                               prnc2exht3zxlrqqcat690tc85cvfuypngh7szx6mk_replays_to_abc.journal
    Transactions span        : 2020-02-22 to 2022-12-07 (1019 days)
    Last transaction         : 2022-12-06 (2 days ago)
    Transactions             : 2539 (2.5 per day)
    Transactions last 30 days: 16 (0.5 per day)
    Transactions last 7 days : 10 (1.4 per day)
    Payees/descriptions      : 1
    Accounts                 : 28 (depth 4)
    Commodities              : 3 (, ABC, BCH)


BCHA fork coin airdrop and policy
---------------------------------

On November 15, 2020, Bitcoin ABC created a spin-off currency by forking
away from the Bitcoin Cash (BCH) currency.

This new coins is referred to as 'ABC' or 'bchabc' in our ledger files.
It is now traded under the symbol XEC and branded as "eCash" on most
exchanges / explorers.

The BCHN project received an amount of ABC's coin equal to the amount
of BCH held in our wallet at the time and entirely converted these (approx.
2064 ABC coins at the time) into around 159.6 BCH by November 19, 2020.

Since then, the project holds virtually no more ABC coins except a minimal
amount which has been received since due to replaying of regular donations
made to our BCH wallet (the 'donation_replay' income position).

If ABC coin donations reach a significant amount, the project may convert
them to BCH in the future as it does not intend to hold them.
At time of this report the ABC coin holdings amounted to less than 4 USD,
the decrease from the last report being due to bear market and price
fluctuation of the ABC coin.


SLP donation address
--------------------

The project has a SimpleLedger (SLP) token donation address.

    simpleledger:qrjv4klcn9z5kqeye3ps64pr6aa4mzux8cy776pm6k

Currently, the project has received and is holding donations of
2,000,000 SPICE 🌶️ , 5 USDH and few other tokens have been
sent to the address. 1000 USDt held previously have been converted
to BCH (refer to Income section above).

As we conduct our payments exclusively in Bitcoin Cash up to now, we have
yet to set up a ledger file to account for token holdings. We may do so for
main tokens like SPICE, but not generally for all random tokens sent our way,
as this would be a way to spam the project.


SmartBCH
--------

The project does not, at this time, keep a SmartBCH wallet.

We will keep you informed of developments and post any related information,
such as a SmartBCH donation address, on the project website.

---

Links
-----

Previous financial overviews:

 - [Financial overview in report of 14 March 2020](https://read.cash/@freetrader/bchn-lead-maintainer-report-2020-03-14-84f2992d#financial-overview)

 - [Financial overview in report of 31 March 2020](https://read.cash/@freetrader/bchn-lead-maintainer-report-2020-03-31-1e0a4c39#financial-overview)

 - [Financial overview in report of 15 April 2020](https://read.cash/@freetrader/bchn-lead-maintainer-report-2020-04-15-449fc115#financial-overview)

 - [BCHN Financial Report 2020-06-03](https://read.cash/@bitcoincashnode/bchn-financial-report-2020-06-03-d2a0232f)

 - [BCHN Financial Report 2020-07-07](https://read.cash/@bitcoincashnode/bchn-financial-report-2020-07-07-d04c7e65)

 - [BCHN Financial Report 2020-09-03](https://read.cash/@bitcoincashnode/bchn-financial-report-2020-09-03-a477d4e2)

 - [BCHN Financial Report 2020-10-20](https://read.cash/@bitcoincashnode/bchn-financial-report-2020-10-20-85ecb54c)

 - [BCHN Financial Report 2020-12-05](https://read.cash/@bitcoincashnode/bchn-financial-report-2020-12-05-db64dbdf)

 - [BCHN Financial Report 2021-02-08](https://read.cash/@bitcoincashnode/bchn-financial-report-2021-02-08-ae9d834d)

 - [BCHN Financial Report 2021-04-17](https://read.cash/@bitcoincashnode/bchn-financial-report-2021-04-17-d849b874)
 
 - [BCHN Financial Report 2021-06-11](https://read.cash/@bitcoincashnode/bchn-financial-report-2021-06-11-b114bb7c)
 
 - [BCHN Financial Report 2021-11-24](https://read.cash/@bitcoincashnode/bchn-financial-report-2021-11-24-2fd7a1a4)

 - [BCHN Financial Report 2022-06-07](https://read.cash/@bitcoincashnode/bchn-financial-report-2021-11-24-64434232)

Flipstarter funding proposal:

 - [English, v1.1](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public/-/raw/master/finance/funding/campaigns/2020-q2-flipstarter/pdf/Bitcoin_Cash_Node_Flipstarter_Funding_Proposal_v1_1.pdf) (PDF)

 - [Chinese, v1.1](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public/-/blob/master/finance/funding/campaigns/2020-q2-flipstarter/pdf/Bitcoin_Cash_Node_Flipstarter_Funding_Proposal_v1_1_CN.pdf) (PDF)

 - [Spanish, 1.1](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public/-/blob/master/finance/funding/campaigns/2020-q2-flipstarter/proposal/proposal-es.mediawiki) (mediawiki)


Getting in touch with BCHN:

 - Website: https://bitcoincashnode.org

 - Development & support Slack chat invite link:
   https://join.slack.com/t/bitcoincashnode/shared_invite/zt-egg3c36d-2cglIrKcbnGpIQFaKFzCWA

 - Telegram: https://t.me/bitcoincashnode (there is a bridge channel to our Slack)

 - IRC channel: Join #bchnode on Freenode (we see messages on our Slack via an IRC bridge channel)

 - Logs of our development Slack: http://logs.bchnode.org/

 - Main development repository on GitLab:
   https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node

 - Easy download link via our website:
   https://bitcoincashnode.org/download.html

 - Release mirrors on Github:
   https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases

 - Donation address: https://bitcoincashnode.org/#donate

 - Follow us on Twitter: https://twitter.com/bitcoincashnode

 - Follow us on Reddit: https://www.reddit.com/r/bchnode
