Test File
=========

This is a test file, for practising
- editing,
- review,-
- translation

It can stay here as long as needed.
Anything in the `draft` folder is not a problem.

You also cannot do anything wrong, even if you delete the entire contents.
The next person can always get them back since they are in a version control system (GitLab)

Happy editing!

🙂🙂🙂 Unicode is of course possible and also necessary for translations.

If you want to edit a file outside of GitLab, it should be as easy as either copy-pasting to a text editor of your choice (but take care that it stays in text format on the way back here!)

Our documents are written in _Markdown_ , a markup language for text.
It comes in different flavors, but their differences are small.
The basic set of **markup** is the same.

[This is a Markup tutorial link.](https://www.w3schools.io/file/markdown-introduction/)

Usually, sites like GitLab or Github display the Markdown in a nicely rendered way (making **bolded text** actually bold instead of showing the markup characters).

You can switch between the rendered view and the raw markdown text on sites like this one. When you download a file or try to edit it in GitLab's Web IDE, you get to work on the raw Markdown text.

We conventially name all our Markdown file names with a '.md' extension.
That applies to documents in this repository ('announcements') but also other repositories in the BCHN project, like the code repository (which does include some documentation files).
